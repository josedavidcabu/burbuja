/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.ufps.colecciones_seed.ListaS;

/**
 *
 * @author Jose
 */
public class EjercicioRepaso {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ListaS<Integer> lista1 = new ListaS();
            lista1.insertarFin(5);
            lista1.insertarFin(30);
            lista1.insertarFin(22);
            lista1.insertarFin(14);
            lista1.insertarFin(4);
            lista1.insertarFin(6);
            lista1.ordenamientoBrubujaNodos();
            System.out.println(lista1.toString());
        } catch (RuntimeException e) {
            System.err.println(e.getMessage());
        }
    }

}
