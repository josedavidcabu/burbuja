/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Punto;
import java.util.Random;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * Clase permite crear una colección de puntos de manera aleatoria
 * @author madar
 */
public class PlanoCartesiano {
    
    
    private VectorGenerico<Punto> myPtos;

    public PlanoCartesiano() {
    }
    
    
    public PlanoCartesiano(int cantidadPuntos) {
        try
        {
            this.myPtos=new VectorGenerico(cantidadPuntos);
            this.crearPuntos_Aleatorios();
            
        }catch(Exception e)
        {
            System.err.println("No se puede crear los puntos-Cantidad inválida");
        
        }
        
        
    }
    
    private void crearPuntos_Aleatorios()
    {
    
        for(int i=0;i<this.myPtos.length();i++)
        {
         float x=(float)(Math.random()*200)+1;
         float y=(float)(Math.random()*200)+1;
         Punto nuevoPunto=new Punto(x,y);
         this.myPtos.add(nuevoPunto);
        }
    }

    @Override
    public String toString() {
        String str="";
        for(int i=0;i<this.myPtos.length();i++)
             str+=this.myPtos.get(i).toString()+"\t";
        return str;
    }
    
    
    
}
