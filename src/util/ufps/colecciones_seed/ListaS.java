/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class ListaS<T> {

    //Inicialización OPCIONAL, SE REALIZA POR CONCEPTO
    private Nodo<T> cabeza = null;
    private int tamanio = 0;

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }

    /**
     * Método para insertar un elemento nuevo en el inicio de la lista simple
     *
     * @param datoNuevo un objeto para la listaS
     */
    public void insertarInicio(T datoNuevo) {
        /**
         * Estrategia computacional(Algoritmo): 1. Crear nodoNuevo=new
         * (dato,cabeza); 2. aumentar tamanio 3. Cambiar cabeza a nuevoNodo
         */

        Nodo<T> nodoNuevo = new Nodo(datoNuevo, this.cabeza);
        this.cabeza = nodoNuevo;
        this.tamanio++;

    }

    public void insertarFin(T datoNuevo) {
        if (this.esVacia()) {
            this.insertarInicio(datoNuevo);
        } else {
            Nodo<T> nodoNuevo = new Nodo(datoNuevo, null);

            try {
                Nodo<T> nodoAnt_Actual = getPos(this.tamanio - 1);
                nodoAnt_Actual.setSig(nodoNuevo);
                this.tamanio++;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public void set(int i, T datoNuevo) {

        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }

    /**
     * Método elimina el nodo que se encuentra en una posición dada
     *
     * @param i posición del nodo en la lista simple
     * @return el objeto almacenado en el nodo con posición i
     */
    public T eliminar(int i) {
        if (this.esVacia()) {
            return null;
        }
        Nodo<T> nodoBorrar = null;
        if (i == 0) {
            nodoBorrar = this.cabeza;
            this.cabeza = this.cabeza.getSig();
        } else {
            try {
                Nodo<T> nodoAnt = this.getPos(i - 1);
                nodoBorrar = nodoAnt.getSig();
                nodoAnt.setSig(nodoBorrar.getSig());

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
        }
        nodoBorrar.setSig(null);
        this.tamanio--;
        return nodoBorrar.getInfo();
    }

    private Nodo<T> getPos(int i) throws Exception {
        if (i < 0 || i >= this.tamanio) {
            throw new Exception("Índice fuera de rango");
        }

        Nodo<T> nodoPos = this.cabeza;

        while (i-- > 0) {
            nodoPos = nodoPos.getSig();

        }
        return nodoPos;
    }

    @Override
    public String toString() {
        String msg = "";

        for (Nodo<T> nodoActual = this.cabeza; nodoActual != null; nodoActual = nodoActual.getSig()) {
            msg += nodoActual.getInfo().toString() + "->";
        }
        return "Cabeza->" + msg + "null";

    }

    public boolean esVacia() {
        return this.cabeza == null;
    }

    /**
     * Elimina de la lista todo dato que esté repetido (NO SE DEJA NINGUNO) NO
     * SE PUEDE LLAMAR N VECES AL MÉTODO REPETIR NO SE PUEDE USAR EL MÉTODO GET
     * O SET NO SE PUEDE USAR EL MÉTODO GETPOS
     *
     * Ejemplo: L=<2,3,4,2,1,9,2,1,3>
     * L.eliminarRepetido()--> L=<4,9>
     */
    public void eliminarRepetido() {
        T elementosVistos[] = (T[]) new Object[this.tamanio];
        boolean repetidos[] = new boolean[this.tamanio];

        int i = this.tamanio;
        int elementosAgregados = 0;
        Nodo<T> nodoActual = this.cabeza;
        Nodo<T> nodoAnterior = null;

        while (i-- > 0) {
            boolean exist = false;
            for (int j = 0; j < elementosAgregados && !exist; j++) {
                if (elementosVistos[j].equals(nodoActual.getInfo())) {
                    repetidos[j] = true;
                    exist = true;
                    nodoAnterior.setSig(nodoActual.getSig());
                    nodoActual.setSig(null);
                    this.tamanio--;
                }
            }
            if (!exist) {
                elementosVistos[elementosAgregados] = nodoActual.getInfo();
                elementosAgregados++;
                nodoAnterior = nodoActual;
                nodoActual = nodoActual.getSig();
            } else {
                nodoActual = nodoAnterior.getSig();
            }
        }

        nodoActual = this.cabeza;
        nodoAnterior = null;
        for (int j = 0; j < elementosAgregados; j++) {
            if (repetidos[j]) {
                if (nodoAnterior == null) {
                    this.cabeza = nodoActual.getSig();
                    nodoActual.setSig(null);
                    nodoActual = this.cabeza;
                } else {
                    nodoAnterior.setSig(nodoActual.getSig());
                    nodoActual.setSig(null);

                    nodoActual = nodoAnterior.getSig();
                }
                this.tamanio--;
            } else {
                nodoAnterior = nodoActual;
                nodoActual = nodoActual.getSig();
            }
        }

    }

    /**
     * Método que permite mover el mayor elemento de la lista al final. El
     * proceso sólo será válido si existe un ÚNICO MAYOR Restricciones: 1. Lista
     * no vacía 2. No se pueden crear nodos ( new Nodo...) 3. SÓLO SE PUEDE
     * REALIZAR EL PROCESO EN UN CICLO 4. No se puede utilizar los métodos:
     * getpos, eliminar o adicionar
     *
     * @return
     */
    public boolean moverMayor_al_fin() {
        /**
         *
         * Casos: 1. L=<> --> L.moverMayor_al_Fin()--> "false" 2. L=<2,4,56,7>
         * --> L.moverMayor() --> L=<2,4,7,56> , "true"-->mitad 3.
         * L=<2,4,56,7,56> --> L.moverMayor() --> L=<2,4,56,7,56>,"false" 4.
         * L=<2> -->L.moverMayor()-->L=<2> , "false" 5. L=<112,4,56,7>-->
         * L.moverMayor()-->L=<4,56,7,112>, "true"--> "cabeza".
         * 6.L=<112,4,56,711>--> L.moverMayor()-->L=<112,4,56,711>, "true"-->
         * "final".
         */

        //Caso 1 y 4
        if (this.esVacia() || this.getTamanio() == 1) {
            throw new RuntimeException("No tiene el tamaño adecuado para realizar el proceso");
        }

        //Referenciando y/o inicializando
        //L={2000,30,40,30,50,4000,1,2000,1,2,50,100,20};--> :(
        Nodo<T> nodoMayor = this.cabeza;
        Nodo<T> nodoAnt_Actual = null;
        boolean repetido = false;
        Nodo<T> nodoAnt_mayor = null;
        for (Nodo<T> nodoActual = this.cabeza.getSig(); nodoActual != null; nodoActual = nodoActual.getSig()) {
            /**
             * esto es un grave grave error: if(nodoactual > nodomayor)
             * nodomayor=nodoactual
             */
            int comparador = ((Comparable) nodoActual.getInfo()).compareTo(nodoMayor.getInfo());
            if (comparador > 0) {
                nodoAnt_mayor = nodoAnt_Actual;
                nodoMayor = nodoActual;
                repetido = false;
            } else {
                if (comparador == 0) {
                    repetido = true;
                }
            }
            nodoAnt_Actual = nodoActual;
        } //nodoActual=null, nodoAnt_actual está en el último
        //Caso 3.
        if (repetido) {
            throw new RuntimeException("Elemento mayor repetido, no se puede realizar el proceso");
        }
        //Caso 5:
        if (nodoMayor.getSig() != null) {
            if (nodoMayor == this.cabeza) {
                this.cabeza = nodoMayor.getSig();

            } else {
                nodoAnt_mayor.setSig(nodoMayor.getSig());

            }

            nodoAnt_Actual.setSig(nodoMayor);
            nodoMayor.setSig(null);
        }
        return !repetido;
    }

    public boolean elementosRepetidos(ListaS<T> listaComparar) {
        boolean repetido = false;
        if (!this.esVacia() || !listaComparar.esVacia()) {

            for (Nodo<T> nodoActual = this.cabeza; nodoActual != null && !repetido; nodoActual = nodoActual.getSig()) {
                for (Nodo<T> x = listaComparar.cabeza; x != null && !repetido; x = x.getSig()) {
                    repetido = ((Comparable) nodoActual.getInfo()).equals(x.getInfo());
                }
            }
        } else {
            throw new RuntimeException("Alguna de las 2 listas esta vacia");
        }
        return repetido;
    }

    public boolean sonIguales(ListaS<T> listaComparar) {
        boolean sonIguales = true;
        if (!this.esVacia() || !listaComparar.esVacia()) {
            Nodo<T> nodoActual2 = listaComparar.cabeza;
            for (Nodo<T> nodoActual = this.cabeza; nodoActual != null && sonIguales; nodoActual = nodoActual.getSig()) {

                sonIguales = ((Comparable) nodoActual.getInfo()).equals(nodoActual2.getInfo());
                nodoActual2 = nodoActual2.getSig();
            }
        } else {
            throw new RuntimeException("Alguna de las 2 listas esta vacia");
        }
        return sonIguales;

    }

    public void ordenamientoBrubujaInfos() {
        int comparador;
        if (this.esVacia()) {
            throw new RuntimeException("Lista vacia");
        } else {
            for (int i = 0; i < this.tamanio-1; i++) {
                for (Nodo<T> nodoActual = this.cabeza; nodoActual.getSig() != null; nodoActual = nodoActual.getSig()) {
                    comparador = ((Comparable) nodoActual.getInfo()).compareTo(nodoActual.getSig().getInfo());
                    if (comparador > 0) {
                        Nodo<T> temporal = new Nodo<>(nodoActual.getInfo(), nodoActual.getSig());
                        nodoActual.setInfo((temporal.getSig().getInfo()));
                        nodoActual.getSig().setInfo(temporal.getInfo());

                    }

                }
            }
        }
    }

    public void ordenamientoBrubujaNodos() {
        if (this.esVacia()) {
            throw new RuntimeException("Lista vacia");
        }
        int comparador;
        Nodo<T> anterior = null;
        for (int i = 0; i < (this.tamanio-1); i++) {
            for (Nodo<T> nodoActual = this.cabeza; nodoActual.getSig() != null; nodoActual = nodoActual.getSig()) {

                comparador = ((Comparable) nodoActual.getInfo()).compareTo(nodoActual.getSig().getInfo());

                if (comparador > 0) {
                    if (nodoActual == this.cabeza) {
                        this.cabeza = nodoActual.getSig();
                        nodoActual.setSig(this.cabeza.getSig());
                        this.cabeza.setSig(nodoActual);
                        nodoActual=this.cabeza;

                    } else {
                        anterior.setSig(nodoActual.getSig());
                        nodoActual.setSig(nodoActual.getSig().getSig());
                        anterior.getSig().setSig(nodoActual);
                        nodoActual=anterior.getSig();
                        
                    }
 
                } 
                anterior=nodoActual;
            }
        }

    }
}
